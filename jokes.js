jokes = [
    'People used to laugh at me when I would say "I want to be a comedian". Well nobody is laughing now.',
    'I hate Russian dolls, they are so full of themselves.',
    'The first time I got a universal remote control, I thought to myself "This changes everything."',
    'Question: What do you call a boomerang that does not come back? Answer: A stick.',
    'I refused to believe my roadworker father was stealing from his job, but when I got home, all the signs were there.',
    'I recently decided to sell my vacuum cleaner, all it was doing was gathering dust',
    'I have not slept for three days, because that would be too long',
    'My grandfather has the heart of a lion and a lifetime ban from the local zoo.',
    'People used to laugh at me when I would say "I want to be a comedian". Well nobody is laughing now.'];