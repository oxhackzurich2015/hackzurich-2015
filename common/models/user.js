var io = require('socket.io')
var ioClient = require('socket.io-client')

module.exports = function (user) {

  // Set the username to the users email address by default.
  user.observe('before save', function setDefaultUsername(ctx, next) {
    if (ctx.instance) {
      if(ctx.isNewInstance) {
        ctx.instance.username = ctx.instance.email;
      }
      ctx.instance.status = 'created';
      ctx.instance.created = Date.now();
    }
    next();
  });

  user.startSocket = function(){

      console.log('starting a socket')

      var socket = ioClient.connect("http://750b19f5.ngrok.io/");
      socket.on('bodyFrame', function(bodyFrame){
        console.log(bodyFrame)
        // ctx.clearRect(0, 0, canvas.width, canvas.height);
        var index = 0;
        bodyFrame.bodies.forEach(function(body){
          if(body.tracked) {
            for(var jointType in body.joints) {
              var joint = body.joints[jointType];
        //       ctx.fillStyle = colors[index];
        //       ctx.fillRect(joint.depthX * 512, joint.depthY * 424, 10, 10);
            }
            //draw hand states
            // updateHandState(body.leftHandState, body.joints[7]);
            // updateHandState(body.rightHandState, body.joints[11]);
            console.log('left hand', body.leftHandState)
            console.log('right hand', body.rightHandState)
            index++;
          }
      });
    });

  }

  user.remoteMethod('startSocket',{
  });

};


