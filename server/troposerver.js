var http = require('http');
var express = require('express');
var app = express();
var tropo_webapi = require('tropo-webapi');
var bodyParser = require('body-parser')
var io = require('socket.io')
var ioClient = require('socket.io-client')
var _ = require('underscore');

// Required to process the HTTP body.
// req.body has the Object while req.rawBody has the JSON string.


app.use(bodyParser());


app.post('/', function(req, res){
    
    var tropo = new TropoWebAPI();
    var name = req.body.session.parameters.name;

    console.log('inside tropo call')

    tropo.call(["sip:844305995@voiptalk.org"]);
    
    var say = new Say("Good morning, "+name+"! Hope you are feeling good this wonderful morning."+ 
        "How would you like your coffee, black, flat white or iced?");
    var choices = new Choices("black, flat white, iced");

    // (choices, attempts, bargein, minConfidence, name, recognizer, required, say, timeout, voice);
    
    tropo.ask(choices, 5, false, null, "coffee", null, null, say, 60, null, null, 0.9);
    
    tropo.on("continue", null, "/afterbreakfast", true);
    
    res.send(TropoJSON(tropo));
    
});

app.post('/afterbreakfast', function(req, res){
    
    var tropo = new TropoWebAPI();

    var answer = req.body['result']['actions']['value'];
    
    tropo.say("You said " + answer +". Excellent choice.");
    tropo.wait(500)

    var say = new Say("Would you like to hear the joke of the day?");
    var choices = new Choices("Yes, no");
    tropo.ask(choices, 5, false, null, "joke", null, null, say, 60, null, null, 0.9);

    tropo.on("continue", null, "/jokeornot", true)
        
    res.send(TropoJSON(tropo));

});

app.post('/jokeornot', function(req, res){
    
    var tropo = new TropoWebAPI();

    var answer = req.body['result']['actions']['value'];
    
    tropo.say("You said " + answer);

    if(answer.toLowerCase() == "yes"){

        var jokes = [
            'People used to laugh at me when I would say "I want to be a comedian". Well nobody is laughing now.',
            'I hate Russian dolls, they are so full of themselves.',
            'The first time I got a universal remote control, I thought to myself "This changes everything."',
            'Question: What do you call a boomerang that does not come back? Answer: A stick.',
            'I refused to believe my roadworker father was stealing from his job, but when I got home, all the signs were there.',
            'I recently decided to sell my vacuum cleaner, all it was doing was gathering dust',
            'I have not slept for three days, because that would be too long',
            'My grandfather has the heart of a lion and a lifetime ban from the local zoo.',
            'People used to laugh at me when I would say "I want to be a comedian". Well nobody is laughing now.'];

        var joke = _.sample(jokes);

        tropo.say('Here is today\'s joke. '+ joke)


    }
    else{
        tropo.say("Oh, I see. We are feeling a bit grumpy today.")
    }

    

    tropo.on("continue", null, "/getNews", true)
        
    res.send(TropoJSON(tropo));

});

// app.post('/askme', function(req, res){
    
   

//     var tropo = new TropoWebAPI();
//     tropo.call("sip:844305995@voiptalk.org");
    
//     var transcription = {"id":"1234", "url":"mailto:einarbmag@gmail.com"};
//     var say = new Say("Please leave a message.");
//     var choices = new Choices(null, null, "#");

//     //function(attempts, bargein, beep, choices, format, maxSilence, maxTime, method, minConfidence, name, required, say, timeout, transcription, url, password, username)
//     tropo.say("Hello, this is the beginning.");
//     tropo.record(null, null, null, choices, null, 7, 60, null, null, "recording", null, say, 10, transcription, "ftp://einarbmag.hostedftp.com/recordcall.wav", "hackzurich", "einarbmag");
//     res.end(TropoJSON(tropo));

    

    
// });

// app.post('/question', function(req, res){

//     console.log('inside /question')
    
//     var tropo = new TropoWebAPI();

//     // var answer = req.body['result']['actions']['value'];
    
//     // console.log('answer: ', answer)

//     console.log('req.body: ', req.body)
    

        
//     res.send(TropoJSON(tropo));

// });

app.post('/getNews', function(req, res){
    
    var tropo = new TropoWebAPI();

    console.log('inside getNews')
 
    tropo.say("On a more serious note, these are the main headlines today.");
    tropo.wait(500);
    tropo.say("Gunman in Oregon college massacre committed suicide.")
    tropo.wait(500);
    tropo.say("Russia says to step up airstrikes in Syria.")
    tropo.wait(500);
    tropo.say("Migrants break into Channel Tunnel as tension mounts in Calais.")

    // (choices, attempts, bargein, minConfidence, name, recognizer, required, say, timeout, voice);
    
       
    res.send(TropoJSON(tropo));
    
});


app.post('/move', function(req, res){
    
    var tropo = new TropoWebAPI();

    console.log('inside tropo move')

    tropo.call(["sip:844305995@voiptalk.org"]);
    
    tropo.say("Hi, please get up and move.")


      

    tropo.on("continue", null, "/waitformove", true);

    res.send(TropoJSON(tropo));
    
    
});

app.post('/waitformove',function(req,res){
    var socket = ioClient.connect("http://750b19f5.ngrok.io/");
      socket.on('bodyFrame', function(bodyFrame){
        console.log(bodyFrame)
        var index = 0;
        bodyFrame.bodies.forEach(function(body){
          if(body.tracked) {
            for(var jointType in body.joints) {
              var joint = body.joints[jointType];
            }
            console.log('left hand', body.leftHandState)
            console.log('right hand', body.rightHandState)
            index++;
            if ( body.leftHandState == 2 || body.rightHandState == 2){
                
                var tropo = new TropoWebAPI();

                tropo.say("Great, you are up!");
                        
                TropoJSON(tropo);
                
            }
          }
      });
    });
      res.next();


});








app.listen(8000);
console.log('Server running on port :8000');
