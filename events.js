events = [
    '1582 – Pope Gregory XIII implements the Gregorian calendar. In Italy, Poland, Portugal, and Spain, October 4 of this year is followed directly by October 15.',
    '1824 – Mexico adopts a new constitution and becomes a federal republic.',
    '1830 – Creation of the Kingdom of Belgium after separation from the Netherlands.',
    '1895 – The first U.S. Open Mens Golf Championship administered by the United States Golf Association is played at the Newport Country Club in Newport, Rhode Island.',
    '1927 – Gutzon Borglum begins sculpting Mount Rushmore.',
    '1957 – Space Race: Launch of Sputnik 1, the first artificial satellite to orbit the Earth.',
    '1965 – Pope Paul VI arrives in New York, the first Pope to visit the United States of America and the Western hemisphere.',
    '1966 – Basutoland becomes independent from the United Kingdom and is renamed Lesotho.',
    '1976 – Official launch of the InterCity 125 high speed train.',
    '1985 – The Free Software Foundation is founded in Massachusetts, United States.',
    '1991 – The Protocol on Environmental Protection to the Antarctic Treaty is opened for signature.',
    '1992 – The Rome General Peace Accords ends a 16-year civil war in Mozambique.',
    '1997 – The second largest cash robbery in U.S. history occurs at the Charlotte, North Carolina office of Loomis, Fargo and Company. A Federal Bureau of Investigation investigation eventually results in 24 convictions and the recovery of approximately 95% of the $17.3 million stolen cash.'];
    