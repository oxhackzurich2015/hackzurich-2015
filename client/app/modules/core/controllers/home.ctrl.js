(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.core.controller:HomeCtrl
   * @description Dashboard
   * @requires $scope
   * @requires $rootScope
   **/
  angular
    .module('com.module.core')
    .controller('HomeCtrl', function ($scope, $rootScope, Tropomodel, User) {
      $scope.count = {};
      $scope.boxes = $rootScope.dashboardBox;

      $scope.startSocket = function(){
        return Tropomodel.move();
      }

      $scope.triggerCall = function(idx){
        $scope.clients[idx].beingWoken = true;
        return Tropomodel.triggerCall();
      }

      var d0 = new Date();
      var d1 = new Date();
      var d2 = new Date();
      d0.setHours(6);
      d0.setMinutes(45);
      d1.setHours(8);
      d1.setMinutes(20);
      d2.setHours(10);
      d2.setMinutes(15);


      $scope.clients = 
       [{name:'Einar',prefs:{move:false, music:false, news:true, jokes:true, calendar:false, coffee:true}, nextAlarm:d0, beingWoken:false},
        {name:'Brynhildur',prefs:{move:true, music:true, news:true, jokes:true, calendar:false, coffee:false}, nextAlarm:d1, beingWoken:false},
        {name:'Thormodur',prefs:{move:true, music:false, news:true, jokes:true, calendar:true, coffee:false}, nextAlarm:d2, beingWoken:false}]

      $scope.changePref = function(userId, pref){
        $scope.clients[userId].prefs[pref] = !$scope.clients[userId].prefs[pref];
      }

      $scope.mytime = new Date();

        $scope.hstep = 1;
        $scope.mstep = 15;

        $scope.options = {
          hstep: [1, 2, 3],
          mstep: [1, 5, 10, 15, 25, 30]
        };

        $scope.ismeridian = true;
        $scope.toggleMode = function() {
          $scope.ismeridian = ! $scope.ismeridian;
        };

        $scope.update = function() {
          var d = new Date();
          d.setHours( 14 );
          d.setMinutes( 0 );
          $scope.mytime = d;
        };

      $scope.triggerAskme = function(){
        return Tropomodel.triggerAskme();
      }

    });

})();
